/*****************************************************************************
* Thesis MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM
*
* MODULENAME.: InfoTask
*
* PROJECT....: MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM
*
*
* Change Log:
*****************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
* 1708011 SQ Module Created
*****************************************************************************/

/***************************** Include files *******************************/
#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "FreeRTOS.h"
#include "Task.h"
#include "queue.h"
#include "semphr.h"
#include "TypeDef.h"
#include "binary.h"
#include "Sysinfo.h"
#include "uartstdio.h"
#include "uart.h"
#include "hw_memmap.h"
#include "CrcCalculator.h"

/*****************************    Defines    *******************************/
#define Read 	0x80

/*****************************   Constants   *******************************/

/*****************************   Variables   *******************************/

/*****************************   Functions   *******************************/
void Transmit(void);



void Transmit(void)
/*****************************************************************************
*   Input    : -
*   Output   : -
*   Function : Transmit the global variables to MATLAB
******************************************************************************/
{
	// Initialize variables
	uint8_t TransmitGlobalVariable[40] = { 0x90, (2*CellInfo.NumberOfCell+(CellInfo.NumberOfTempSense)*CellInfo.NumberOfBmsBoard+8)+1};
	uint8_t Count = 0;
	uint8_t OperationMode;

	// Check semaphor, if not aviable block task until it is. 
	xSemaphoreTake(CellInfoSemph,portMAX_DELAY);
	// Get Global variable data and prepare for transmit to MATLAB
	for ( int i = 0; i < (CellInfo.NumberOfCell); i++ )
	{
		TransmitGlobalVariable[2*i+2] 	= (uint8_t)Voltage[i];
		TransmitGlobalVariable[2*i+3] 	= (uint8_t)(Voltage[i] >> 8);
		Count = 2*i+3;
	}
	for ( int i = 0; i < (CellInfo.NumberOfTempSense*CellInfo.NumberOfBmsBoard); i++ )
	{
		TransmitGlobalVariable[1+Count] = Temperature[i]+10;
		Count = 1+Count;
	}
	TransmitGlobalVariable[1+Count] = (uint8_t)StateOfCharge;
	Count = 1+Count;
	TransmitGlobalVariable[1+Count] = (uint8_t)StateOfHealth;
	Count = 1+Count;
	TransmitGlobalVariable[1+Count] = (uint8_t)Current;
	Count = 1+Count;
	TransmitGlobalVariable[1+Count] = (uint8_t)(Current >> 8);
	Count = 1+Count;
	TransmitGlobalVariable[1+Count] = (uint8_t)Fault;

	if ( OpMode.HibernationMode == TRUE )
	{
		OperationMode = 1;
	}
	else if ( OpMode.UpsMode == TRUE )
	{
		OperationMode = 2;
	}else if ( OpMode.StandAloneMode == TRUE )
	{
		OperationMode = 3;
	}

	Count = 1+Count;
	TransmitGlobalVariable[1+Count] = (uint8_t)OperationMode;
	Count = 1+Count;
	TransmitGlobalVariable[1+Count] = (uint8_t)SystemVoltage;
	Count = 1+Count;
	TransmitGlobalVariable[1+Count] = (uint8_t)(SystemVoltage >> 8);


	xSemaphoreGive(CellInfoSemph);

	for ( int i = 0; i < TransmitGlobalVariable[1]+2; i++)
	{ 
		// Transmit all global variables to MATLAB 
		UARTCharPut(UART0_BASE, TransmitGlobalVariable[i]);
	}
}


void Info_task(void *pvParameters)
/*****************************************************************************
*   Input    : FreeRTOS parameter and Cell information.
*   Output   : -
*   Function : Task to handle information to transmit data.
******************************************************************************/
{
	// Initialize value. 
	uint8_t Request = 0;
	uint16_t CellSoc[CellInfo.NumberOfCell];
	uint8_t RequestUpsMode[5] = {0x02, 0x02};
	bool_t OperationMode;

	while(1)
	{
		// wait for an UpsEvent
		xQueueReceive(UpsEventQueue,&Request,portMAX_DELAY);


		if ( Request == Read )
		{
			// If Ups is data request - transmit data. 
			Transmit();

		}else
		{
			// Else check SystemSoc
			xQueueSendToBack(UpsStatusEventQueue,&Request,portMAX_DELAY);
		}
	}
}

/****************************** End Of Module *******************************/
