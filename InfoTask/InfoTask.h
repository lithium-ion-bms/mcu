/*****************************************************************************
* Thesis MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM
*
* MODULENAME.: InfoTask
*
* PROJECT....: MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM
*
* DISCRIPTION: This task will send data out through UART.
*
*
* Change Log:
*****************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
* 11-08-2017 SQ Module Created
*
*****************************************************************************/

#ifndef INFOTASK_H_
  #define INFOTASK_H_

/***************************** Include files *******************************/

/*****************************    Defines    *******************************/

/********************** External declaration of Variables ******************/

/*****************************   Constants   *******************************/

/*************************  Function interfaces ****************************/

void Info_task(void *pvParameters);


/****************************** End Of Module *******************************/
#endif
