/*****************************************************************************
* Thesis MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
* MODULENAME.: bq76PL455ADriver.c
*
* PROJECT....: MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
* DISCRIPTION:
* Driver to handle communication and Data Aqusition from the BMS evaluation board.
*
* Change Log:
*****************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
* 170920 SQ 	Module created
*****************************************************************************/

#ifndef bq76pl455adriver_h_
  #define bq76pl455adriver_h_

/***************************** Include files *******************************/

/*****************************    Defines    *******************************/

/********************** External declaration of Variables ******************/

/*****************************   Constants   *******************************/

/*************************  Function interfaces ****************************/

eReturn_t InitBmsBoard();
void TransmitData(uint8_t Size, uint8_t Data[]);
void ReceivedData(uint8_t *Data);
void ChangeNumberOfBmsBoard(uint8_t NumberOfBmsBoard);
void ChangeNumberOfCell(uint8_t NumberOfCell);
void ChangeNumberOfTemperatureSense(uint8_t NumberOfTemperatureSense);
void GetData(uint8_t *pData);
void RequestData(void);
void CellBalCmd(uint8_t CellNumber, uint8_t BoardNumber);

/****************************** End Of Module *******************************/
#endif
