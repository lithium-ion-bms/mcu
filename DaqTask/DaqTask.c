/*****************************************************************************
* Thesis MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
* MODULENAME.: DaqTask.h
*
* PROJECT....:  MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
*
* Change Log:
*****************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
* 170911 SQ 	Module created
*****************************************************************************/

/***************************** Include files *******************************/
#include <stdint.h>
#include "stdbool.h"
#include "FreeRTOS.h"
#include "Task.h"
#include "queue.h"
#include "semphr.h"
#include "TypeDef.h"
#include "binary.h"
#include "Sysinfo.h"
#include "GPIO.h"
#include "hw_memmap.h"
#include "sysctl.h"
#include "tm4c123gh6pm.h"
#include "pwm.h"

#include "DaqTask.h"
#include "CrcCalculator.h"
#include "uart.h"
#include "uartstdio.h"
#include "bq76PL455ADriver.h"
#include "adcinterface.h"
#include "buttons.h"


/*****************************    Defines    *******************************/
#define DaqFreq200Hz	5
#define Scale 			1/16384
/*****************************   Constants   *******************************/

/*****************************   Variables   *******************************/
bool_t Opmode;
static uint8_t Data[16][51];

/*****************************   Functions   *******************************/

/*****************************   Prototypes   ******************************/
void Daq_task(void *pvParameters);
void PollButton_Task(void *pvParameters);
void CollectData(uint8_t *pData);
void UpdateGlobalVariable(uint8_t *pData, uint32_t *pAdcValue, uint8_t BMSBoard);
void ReadAdc(uint32_t *AdcValue);
int16_t CheckTemp(uint16_t VNtc, uint16_t VRef);


int16_t CheckTemp(uint16_t VNtc, uint16_t VRef)
/*****************************************************************************
*   Input    : Voltage reading from NTC, voltage reference
*   Output   : Temperature
*   Function : Calculate the temperature
******************************************************************************/
{
	int16_t pTemperature= 0;
	static uint16_t RAdc = 10000;  	// Voltage divider resistor value before ADC.
	uint32_t RTemp; 				// Voltage Divider VNtc = Vref*(RAdc/(RAdc+NtcRes))
	static uint16_t NtcRes[81] = {
									// NTC Resistor value, 1 degree resolution. All decimal ruled out.
							55046, 52468, 49890, 47312, 44734, 42157, 40236, 38315, 36395, 34474,
							32554, 31111, 29668, 28225, 26782, 25339, 24245, 23152, 22058, 20965,
							19872, 19037, 18202, 17367, 16532, 15698, 15056, 14414, 13772, 13130,
							12488, 11990, 11492, 10995, 10497, 10000, 9611,  9223,  8835,  8447,
							8059,  7754,  7449,  7144,  6839,  6535,  6294,  6053,  5812,  5571,
							5330,  5138,  4946,  4755,  4563,  4372,  4218,  4065,  3911,  3758,
							3605,  3481,  3358,  3235,  3112,  2989,  2889,  2789,  2689,  2589,
							2490,  2408,  2327,  2246,  2165,  2084,  2017,  1951,  1885,  1819,  1753
						  };
	VNtc++;
	RTemp = (VNtc*RAdc/(VRef-VNtc));
	while ( NtcRes[pTemperature] >= RTemp )
	{
		pTemperature++;
	}

	pTemperature -= 10;
	return pTemperature;
}


void CollectData(uint8_t *pData)
/*****************************************************************************
*   Input    : Non
*   Output   : Pointer to data array where the sense value needs to be stored.
*   Function : Collect the data from the battery cells.
******************************************************************************/
{
	// Init Variables
	uint8_t k = CellInfo.NumberOfBmsBoard-1;
	uint8_t count = 0;
	
	// Get Data from DSP
	StateOfCharge = UARTCharGetNonBlocking(UART3_BASE);
	StateOfHealth = UARTCharGetNonBlocking(UART3_BASE);
	Current = UARTCharGetNonBlocking(UART3_BASE);
	Current = Current << 8;
	Current |= UARTCharGetNonBlocking(UART3_BASE);
	
	// Request and store data.
	/*RequestData();

		GetData(pData);
		for ( int j = 0; j < (pData[0]+4)*CellInfo.NumberOfBmsBoard; j++)
		{
			if ( j == (pData[0]+4)*((CellInfo.NumberOfBmsBoard)-k) )
			{
				k--;
				count = 0;
			}
			Data[k][count] = pData[j];
			count++;
		}*/
}


void UpdateGlobalVariable(uint8_t *pData, uint32_t *pAdcValue, uint8_t BMSBoard)
/*****************************************************************************
*   Input    : Pointer to the received sense value
*   Output   : Update the global value containing the sense value.
*   Function : Convert the received sense value to mUnit
******************************************************************************/
{
	static uint32_t Temp = 0;
	static int16_t TempCur = 0;
	uint32_t ReferenceVoltage = 0;
	xSemaphoreTake(CellInfoSemph, portMAX_DELAY);
	// Loop through the received Data and calculate the value in mUnit.

	for ( int j = 0; j < CellInfo.NumberOfBmsBoard; j++)
	{
		for (int i = 0; i < CellInfo.NumberOfCellPerString; i++)
		{
			Temp = Data[j][2*i+1] << 8 | Data[j][2*i+2];
			Voltage[(j+1)*CellInfo.NumberOfCellPerString-((i+1))] = (Temp)*MiliScale;
		}
		for (int i = 0; i < CellInfo.NumberOfTempSense; i++)
		{
			Temp = Data[j][CellInfo.NumberOfCellPerString*2+(2*i+1)] << 8 | Data[j][CellInfo.NumberOfCellPerString*2+(2*i+2)];
			Temperature[i+(j*CellInfo.NumberOfTempSense)] = CheckTemp(Temp*MiliScale,5000);
		}
	}
	TempCur = ((pAdcValue[0]*3300/4096)); //*66 25mV/A
	if ( TempCur > 1650 )
	{
		TempCur = ((TempCur-1650)*1)*0.6;
	}else
	{
		TempCur = ((TempCur-1650)*(-1))*0.6;
	}
	SystemVoltage = TempCur;//pAdcValue[1]*3300/4096*6.6837;
	xSemaphoreGive(CellInfoSemph);

	UARTCharPut(UART3_BASE,0xff);
	UARTCharPut(UART3_BASE,(uint8_t)(TempCur>>8));
	UARTCharPut(UART3_BASE,(uint8_t)(TempCur));
	UARTCharPut(UART3_BASE,(uint8_t)((SystemVoltage/6)>>8));
	UARTCharPut(UART3_BASE,(uint8_t)(SystemVoltage/6));


}


void ReadAdc(uint32_t *pAdcValue)
/*****************************************************************************
*   Input    : - 
*   Output   : Pointer to the address where the ADC readin is to be stored. 
*   Function : Read the ADC. 
******************************************************************************/
{
	switch(Opmode)
	{
		case TRUE: // Operation mode is hibernation so only selective data needs to be collected.

			pAdcValue[0] = AdcGet(ADC0_BASE, 0); // Get current
			AdcDisable();
		break;
		case FALSE:  // Operation mode is not in hibernation so all data needs to be collected.
			pAdcValue[0] = AdcGet(ADC0_BASE, 0); // Get current
			pAdcValue[1] = AdcGet(ADC1_BASE, 0); // Get Voltage
			AdcDisable();
		break;
		default:
		break;
	}

}

uint8_t CheckButton()
/*****************************************************************************
*   Input    : - 
*   Output   : Button status 
*   Function : Check the status of the button
******************************************************************************/
{
	uint8_t Return;
	uint8_t pui8Delta;
	uint8_t pui8RawState;
	uint8_t status;

	status = pui8RawState & 0x11;

	// State Machine to determine status of the button
	switch ( status )
	{
	case (0x10):
		ButtonsPoll(pui8Delta, &pui8RawState);
		if ((pui8RawState & 0x11) == 0x00)
		{
			Return = 0x01;
			status = 0x00;
		}else if ((pui8RawState & 0x11) == 0x01)
		{
			Return = 0x00;
		}
		break;
	case 0x01:
		ButtonsPoll(pui8Delta, &pui8RawState);
		if ((pui8RawState & 0x11) == 0x00)
		{
			Return = 0x02;
			status = 0x00;
		}else if ((pui8RawState & 0x11) == 0x10)
		{
			Return = 0x00;
		}
		break;
	default:
		status = ButtonsPoll(pui8Delta, &pui8RawState);
		Return = 0x00;
		break;
	}

return Return;
}


void Daq_task(void *pvParameters)
/*****************************************************************************
*   Input    : Task parameters
*   Output   : Task parameters
*   Function : Task to  handle the DAQ
******************************************************************************/
{
	uint32_t AdcValue[2];
	uint8_t test = 0;
	uint8_t Test;
	uint16_t *Location;
	uint8_t WhatDoIKnow;
	uint32_t HibernationData = 0;
	
	while(1)
	{
		AdcEnable();

		xSemaphoreTake(OpModeSemph, portMAX_DELAY);
		Opmode = OpMode.HibernationMode;
		xSemaphoreGive(OpModeSemph);


		Test = CheckButton();
		if ( (Test == 0x01) && (OpMode.HibernationMode == FALSE) )
		{
			UARTCharPut(UART3_BASE,0xe1);
			OpMode.HibernationMode = TRUE;
			OpMode.StandAloneMode = FALSE;
			OpMode.UpsMode = FALSE;
			StopSystem();
			HibernationData = 1;
		    HibernateDataSet(HibernationData, 1);
		}else if ( (Test == 0x01) && (OpMode.HibernationMode == TRUE) )
		{
			UARTCharPut(UART3_BASE,0xe0);
			OpMode.HibernationMode = FALSE;
			OpMode.StandAloneMode = TRUE;
			OpMode.UpsMode = FALSE;
			StopSystem();
		}

		CollectData(Data); // Collect all data.
		ReadAdc(AdcValue);
		UpdateGlobalVariable(Data,AdcValue,0); // Update global variables.

		if ( Opmode == TRUE )
		{
			xQueueSendToBack(EnableHibernationEventQueue,&test,portMAX_DELAY);
			UARTCharPut(UART3_BASE,0xe1);
		}else
			UARTCharPut(UART3_BASE,0xe0);


		vTaskDelay(DaqFreq200Hz / portTICK_RATE_MS); // wait 5 ms.
	}

}
/****************************** End Of Module *******************************/
