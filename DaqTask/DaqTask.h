/*****************************************************************************
* Thesis MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
* MODULENAME.: DaqTask.h
*
* PROJECT....: MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
* DISCRIPTION: Get the data from the BMS evaluation board and ADC, and store 
				the data in global variables. 
*
* Change Log:
*****************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
* 170911 SQ 	Module created
*****************************************************************************/

#ifndef DAQTASK_H_
  #define DAQTASK_H_

/***************************** Include files *******************************/

/*****************************    Defines    *******************************/

/********************** External declaration of Variables ******************/

/*****************************   Constants   *******************************/

/*************************  Function interfaces ****************************/

void Daq_task(void *pvParameters);

/****************************** End Of Module *******************************/
#endif
