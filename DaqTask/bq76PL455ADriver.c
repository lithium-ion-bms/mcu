/*****************************************************************************
* Thesis MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
* MODULENAME.: bq76PL455ADriver.c
*
* PROJECT....: MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
* DISCRIPTION:
* Driver to handle communication and Data Aqusition from the bms evaluation board.
*
* Change Log:
*****************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
* 170920 SQ 	Module created
*****************************************************************************/

/***************************** Include files *******************************/
#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "FreeRTOS.h"
#include "Task.h"
#include "queue.h"
#include "semphr.h"
#include "TypeDef.h"
#include "binary.h"
#include "Sysinfo.h"
#include "CrcCalculator.h"
#include "UARTStdio.h"
#include "Uart.h"
#include "bq76PL455ADriver.h"
#include "hw_memmap.h"

/*****************************    Defines    *******************************/
#define Cell CellInfo.NumberOfCell
#define Row  12
#define Col  10
/*****************************   Constants   *******************************/

/*****************************   Variables   *******************************/
uint8_t ChangeConfig[9] = { 0x94, 0x00, 0x03, 0x00, 0x3f, 0x07, 0x00 };
uint8_t TopStack = 0;
uint8_t ButtonStack = 0;
/*****************************   Functions   *******************************/

/*****************************   Prototype   *******************************/
eReturn_t InitBmsBoard();
void TransmitData(uint8_t Size, uint8_t *Data);
void ReceivetData(uint8_t *Data);
void ChangeNumberOfBmsBoard(uint8_t NumberOfBmsBoard);
void ChangeNumberOfCell(uint8_t NumberOfCell);
void GetData(uint8_t *pData);
void RequestData(void);

void RequestData(void)
/*****************************************************************************
*   Input    : -
*   Output   : -
*   Function : Send a request to the BMS evaluation board, to collect and 
				transmit data. 
******************************************************************************/
{
	uint8_t GetCmd[5] = {0xE1, 0x02, CellInfo.NumberOfBmsBoard-1};

	CrcCalculator(GetCmd, 3);
	TransmitData(5, GetCmd);
}

void GetData(uint8_t *Data)
/*****************************************************************************
*   Input    : -
*   Output   : The data that are requested from the BMS evaluation board. 
*   Function : Collect the data from the BMS evaluation board.
******************************************************************************/
{
	while(UARTCharsAvail(UART5_BASE) == FALSE){}
	Data[0] = UARTCharGetNonBlocking(UART5_BASE);

	for ( int i = 1; i < (Data[0]+4)*CellInfo.NumberOfBmsBoard; i++)
		{
			while(UARTCharsAvail(UART5_BASE) == FALSE);
			Data[i] = UARTCharGetNonBlocking(UART5_BASE);
		}
}

eReturn_t InitBmsBoard()
/*****************************************************************************
*   Input    : -
*   Output   : -
*   Function : Initialize and setup the BMS evaluation board.
******************************************************************************/
{

	uint8_t Size;
	uint8_t Data[Col];
	static uint8_t InitSequence[Row][Col] =
	{
		/*
		 * Initialize the BMS board, the protocol is as follows:
		 * 		5		4		3		2		1		0
		 * **************************************************
		 * *   CFI	*	DA	*	RA	*	DC	*  CRCL	*  CRCM	*
		 * **************************************************
		 * CFI byte =
		 * bit		7		  6	  5	    4		3			2	1	0
		 * ******************************************************************
		 * * FRM_type * 	REQ_TYPE 		* ADDR_SIZE *	DATA_SIZE		*
		 * ******************************************************************
		 * these sequence will set the following:
		 *
		 */
			{0xF2, 0x10, 0x10,0xE0},					// 1 //
			{0xF1, 0x0E, 0x10},							// 2 //
			{0xF1, 0x0C, 0x08},							// 3 //
			{0xF1, 0x0A, 0x00},							// 4 //
			{0xF1, 0x0A, 0x01},							// 5 //
			{0xF1, 0x0A, 0x02},							// 6 //
			{0x92, 0x01, 0x10, 0x10, 0x20},				// 7 //
			{0x92, 0x00, 0x10, 0x10, 0xC0},				// 8 //
			{0xF4, 0x03, 0x00, 0x3f, 0x07, 0x00}, 		// 9 //
			{0xF1, 0x07, 0x7a}, 						// 10 //
			{0xF1, 0x0d, 0x06},							// 11 //
			{0xF1, 0x0f, 0x80}, 						// 12 //
	};
	uint8_t ReqType = 0;

	// Calculate CRC 
	for ( int i = 0; i < Row; i++)
	{
		if ( ( InitSequence[i][0] & 0xf0 ) == 0xf0 )
		{
			ReqType = 2;
		}else
		{
			ReqType = 3;
		}
		Size = ((InitSequence[i][0] & 0x7)+ReqType);
		for (int j = 0; j < Size; j++)
		{
			Data[j] = InitSequence[i][j];
		}
		CrcCalculator(Data, Size);
		for (int j=0; j < (Size + ReqType); j++)
		{
			InitSequence[i][j] = Data[j];
		}
	}
	// Transmit the setting.
	for (int i = 0; i<Row; i++)
	{
		if ( ( InitSequence[i][0] & 0xf0 ) == 0xf0 )
				{
					ReqType = 4;
				}else
				{
					ReqType = 5;
				}
		Size = ((InitSequence[i][0] & 0x7)+ReqType);
		for (int j = 0; j < Size; j++)
		{
			Data[j] = InitSequence[i][j];
		}
		TransmitData(Size,Data);
	}

	 return eOk;
}


void TransmitData(uint8_t Size, uint8_t *Data)
/*****************************************************************************
*   Input    : Size of the data to be transmitted, the data to transmit. 
*   Output   : -
*   Function : Transmit the data to the BMS board. 
******************************************************************************/
{
	for (int i = 0; i < Size; i++ )
	{
		UARTCharPut(UART5_BASE, Data[i]);

	}
}

void ReceivedData(uint8_t *Data)
/*****************************************************************************
*   Input    : -
*   Output   : Data that are received. 
*   Function : Recieve data from the BMS evaluation board. 
******************************************************************************/
{
		for (uint8_t i = 0; i < (Data[0]+4); i++ )
		{
			while(UARTCharsAvail(UART5_BASE) == FALSE);
			Data[i] = UARTCharGetNonBlocking(UART5_BASE);
		}
}




void ChangeNumberOfBmsBoard(uint8_t NumberOfBmsBoard)
/*****************************************************************************
*   Input    : Number of BMS evaluation board. 
*   Output   : -
*   Function : Configure the stack to contain more or less BMS evaluation board
*				in the stack. RESERVED FOR LATER USE
******************************************************************************/
{
}

void ChangeNumberOfTemperatureSense(uint8_t NumberOfTemperatureSense)
/*****************************************************************************
*   Input    : Number of Temperature. 
*   Output   : -
*   Function : Set a Number of Temperature sensor. 
******************************************************************************/
{
	uint8_t Newconfig = 0;

	Newconfig |= 1;

	for (int i = 0; i < NumberOfTemperatureSense; i++)
	{
		Newconfig = Newconfig << 1;
		Newconfig |= 1;
	}

	ChangeConfig[5] = Newconfig;

	CrcCalculator(ChangeConfig, 7);
	TransmitData(9, ChangeConfig);

}

void ChangeNumberOfCell(uint8_t NumberOfCell)
/*****************************************************************************
*   Input    : Number of cell. 
*   Output   : -
*   Function : Set a Number of cell on each BMS evaluation board. 
******************************************************************************/
{
	uint16_t Newconfig = 0;

	Newconfig |= 1;

	for (int i = 1; i < NumberOfCell; i++)
	{
		Newconfig = Newconfig << 1;
		Newconfig |= 1;
	}

	ChangeConfig[3] = (uint8_t) (Newconfig >> 8);
	ChangeConfig[4] = (uint8_t) Newconfig;

	CrcCalculator(ChangeConfig, 7);
	TransmitData(9, ChangeConfig);

}


void CellBalCmd(uint8_t CellNumber, uint8_t BoardNumber)
/*****************************************************************************
*   Input    :  Cell to balance, and the board where the cell is attached. 
*   Output   : -
*   Function : Drive transistor to saturation, to enable cell balancing. 
******************************************************************************/
{
	uint16_t CellBit = 1;
	uint8_t BalCmd[7] = { 0x92, 0x00, 0x14, 0x00, 0x00 };

	BalCmd[1] = BoardNumber;
	CellBit = CellBit << ((CellNumber-1)-CellInfo.NumberOfCellPerString*BoardNumber);
	BalCmd[3] = (uint8_t)CellBit;
	BalCmd[4] = (uint8_t)(CellBit >> 8);

	CrcCalculator(BalCmd, 7);
	TransmitData(7, BalCmd);

}



/****************************** End Of Module *******************************/
