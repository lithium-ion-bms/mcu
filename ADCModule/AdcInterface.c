/*****************************************************************************
* Thesis MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
* MODULENAME.:  AdcInterface.c
*
* PROJECT....:  MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM
*
*
* Change Log:
*****************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
* 171012  SQ   Module created
*****************************************************************************/

/***************************** Include files *******************************/
#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "FreeRTOS.h"
#include "Task.h"
#include "queue.h"
#include "semphr.h"
#include "TypeDef.h"
#include "binary.h"
#include "Sysinfo.h"
#include "adc.h"
#include "hw_memmap.h"
#include "sysctl.h"
#include "UARTstdio.h"
#include "EmgBrkTask.h"
#include "gpio.h"

/*****************************    Defines    *******************************/

/*****************************   Constants   *******************************/

/*****************************   Variables   *******************************/

/*****************************   Functions   *******************************/
uint32_t AdcGet(uint32_t AdcBase, uint8_t NumberOfSamples);


eReturn_t InitAdc(void)
/*****************************************************************************
*   Input    : Non
*   Output   : succes status of the init
*   Function : Initialize the ADC
******************************************************************************/
{
	eReturn_t Return = eOk;


	SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
	while(!SysCtlPeripheralReady(SYSCTL_PERIPH_ADC0)){}


	// Setup ADC Channel 0 Comparator 0
	ADCSequenceConfigure(ADC0_BASE, 3, ADC_TRIGGER_ALWAYS , 3);
	ADCSequenceStepConfigure(ADC0_BASE, 3, 0, ADC_CTL_END | ADC_CTL_CH0 |  ADC_CTL_CMP0);
	ADCSequenceEnable(ADC0_BASE, 3);


	// Setup ADC Channel 1
	ADCSequenceConfigure(ADC0_BASE, 0, ADC_TRIGGER_PROCESSOR , 0);
	ADCSequenceStepConfigure(ADC0_BASE, 0, 0, ADC_CTL_END | ADC_CTL_CH1 );
	ADCSequenceEnable(ADC0_BASE, 0);



	// Setup comparator with interrupt
	ADCComparatorConfigure(ADC0_BASE, 0, ADC_COMP_TRIG_NONE | ADC_COMP_INT_HIGH_HONCE); // ADC0_BASE,ADC_CTL_CMP0,ADC_COMP_TRIG_NONE|ADC_COMP_INT_HIGH_ALWAYS
	ADCComparatorRegionSet(ADC0_BASE, 0, 0xDDD, 0xEEE); //ADC0_BASE, ADC_CTL_CMP0, 0, 0xff
	ADCComparatorIntEnable(ADC0_BASE, 3);
	ADCIntRegister(ADC0_BASE, 3, &CurrentISR);


	SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC1);
	while(!SysCtlPeripheralReady(SYSCTL_PERIPH_ADC1)){}

	// Setup ADC Channel 4
	//ADCSequenceConfigure(ADC1_BASE, 3, ADC_TRIGGER_ALWAYS , 0);
	ADCSequenceStepConfigure(ADC1_BASE, 0, 0,  ADC_CTL_END | ADC_CTL_CH4 ); //| ADC_CTL_CMP1);
	ADCSequenceEnable(ADC1_BASE, 0);

	// Setup ADC Channel 2 Comparator 1
	ADCSequenceConfigure(ADC1_BASE, 3, ADC_TRIGGER_ALWAYS , 3);
	ADCSequenceStepConfigure(ADC1_BASE, 3, 0, ADC_CTL_END | ADC_CTL_CH2 | ADC_CTL_CMP1 ); //| ADC_CTL_CMP1);
	ADCSequenceEnable(ADC1_BASE, 3);

	return Return;
}

uint32_t AdcGet(uint32_t AdcBase, uint8_t SeqNumber)
/*****************************************************************************
*   Input    : The ADC that should sample.
*   Output   : Pointer to the data where the sampled value is to be stored.
*   Function : Read the ADC and store it in pData
******************************************************************************/
{
	uint32_t Data = 0;
	ADCProcessorTrigger(AdcBase, SeqNumber);
	ADCSequenceDataGet(AdcBase, SeqNumber, &Data);
	return Data;

}

void AdcEnable(void)
/*****************************************************************************
*   Input    :
*   Output   :
*   Function :
******************************************************************************/
{
	GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_7, GPIO_PIN_7);
}

void AdcDisable(void)
/*****************************************************************************
*   Input    :
*   Output   :
*   Function :
******************************************************************************/
{
	GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_7,(0x00));
}

/****************************** End Of Module *******************************/
