/*****************************************************************************
* Thesis MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
* MODULENAME.: ADCModule.h
*
* PROJECT....: MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM
*
* DISCRIPTION: Interface for the ADC driver.
*
*
* Change Log:
*****************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
* 271012 SQ 	Module created
*****************************************************************************/

#ifndef AdcInterface_H_
  #define AdcInterface_H_

/***************************** Include files *******************************/

/*****************************    Defines    *******************************/

/********************** External declaration of Variables ******************/

/*****************************   Constants   *******************************/

/*************************  Function interfaces ****************************/

eReturn_t InitAdc(void);
uint32_t AdcGet(uint32_t AdcBase, uint8_t NumberOfSamples);
void AdcEnable();
void AdcDisable();

/****************************** End Of Module *******************************/
#endif
