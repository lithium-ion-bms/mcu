/*****************************************************************************
* Thesis MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
* MODULENAME.: HibernationTask.c
*
* PROJECT....: MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
*
* Change Log:
*****************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
* 171120	SQ	Module Create 
*****************************************************************************/

/***************************** Include files *******************************/
#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "FreeRTOS.h"
#include "Task.h"
#include "queue.h"
#include "semphr.h"
#include "TypeDef.h"
#include "binary.h"
#include "Sysinfo.h"
#include "hibernate.h"
#include "hw_hibernate.h"
#include "sysctl.h"
#include "GPIO.h"
#include "hw_memmap.h"
#include "buttons.h"
#include "uart.h"

/*****************************    Defines    *******************************/
#define HibernationTimeSec 	5
#define HibernationRTCInit	0

/*****************************   Constants   *******************************/

/*****************************   Variables   *******************************/

/*****************************   Functions   *******************************/


eReturn_t InitHibernation()
/*****************************************************************************
*   Input    : -
*   Output   : Status of initialization
*   Function : Initialize the hibernation
******************************************************************************/
{
	uint32_t ui32Status;
	uint32_t pui32NVData[64];
	uint32_t HibernationData = 0;

    HibernateEnableExpClk(SysCtlClockGet());
    HibernateGPIORetentionEnable();
   	HibernateRTCSet(0);
   	HibernateRTCEnable();
   	HibernateRTCMatchSet(0, HibernateRTCGet() + 5);
   	HibernateWakeSet(HIBERNATE_WAKE_PIN |HIBERNATE_WAKE_RTC);

    return eOk;
}

void Hibernation_task(void *pvParameters)
/*****************************************************************************
*   Input    : -
*   Output   : -
*   Function : Hibernation task, wait for activations signal. 
******************************************************************************/
{
	uint32_t ui32Status;
	uint32_t pui32NVData[64];
	uint32_t HibernationData = 0;
    uint8_t SleepRequest[5] = {0xF1, 0x0C, 0x40};
    uint8_t Thrashcan;

	while(1)
	{
		
		xQueueReceive(EnableHibernationEventQueue,&Thrashcan,portMAX_DELAY);
	
		if ( OpMode.HibernationMode == TRUE )
		{

			GPIOPinWrite(GPIO_PORTF_BASE,( GPIO_PIN_1 | GPIO_PIN_2 |GPIO_PIN_3 ),(0x00));
			GPIOPinWrite(GPIO_PORTA_BASE,GPIO_PIN_3, 0x08);
			GPIOPinWrite(GPIO_PORTA_BASE,GPIO_PIN_2, 0x00);
			vTaskDelay(50 / portTICK_RATE_MS);
			CrcCalculator(SleepRequest, 3);
			TransmitData(5 ,SleepRequest);
			vTaskDelay(50 / portTICK_RATE_MS); 

			HibernationData = 1;

		    HibernateDataSet((uint32_t*)&HibernationData, 1); // STore the hibernation data. 

			HibernateRequest();
			vTaskSuspendAll();

			while(1){}; // wait for power down
		}

	}

}

/****************************** End Of Module *******************************/
