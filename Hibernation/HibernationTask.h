/*****************************************************************************
* Thesis MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
* MODULENAME.: HibernationTask.c
*
* PROJECT....: MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
* DISCRIPTION: Module to handle the Hibernation mode. 
*
*
* Change Log:
*****************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
* 171120	SQ	Module Create 
*****************************************************************************/

#ifndef HIBERNATIONTASK_H_
  #define HIBERNATIONTASK_H_

/***************************** Include files *******************************/

/*****************************    Defines    *******************************/

/********************** External declaration of Variables ******************/

/*****************************   Constants   *******************************/

/*************************  Function interfaces ****************************/

eReturn_t InitHibernation();
void Hibernation_task(void *pvParameters);

/****************************** End Of Module *******************************/
#endif
