/*****************************************************************************
* Thesis MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
* MODULENAME.: 
*
* PROJECT....:  MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM
*
*
* Change Log:
*****************************************************************************
* Date    	Id  Change
* 171120	SQ	Module created
* --------------------
*
*****************************************************************************/

/***************************** Include files *******************************/
#include <stdint.h>
#include "pin_map.h"
#include "tm4c123gh6pm.h"
#include "TypeDef.h"
#include "FreeRTOS.h"
#include "Task.h"
#include "queue.h"
#include "semphr.h"
#include "systick.h"
#include "tmodel.h"
#include "sysctl.h"
#include "hw_memmap.h"
#include "uart.h"
#include "uartstdio.h"
#include "rom.h"
#include "gpio.h"
#include "adc.h"
#include "hibernate.h"

#include "status_led.h"
#include "InfoTask.h"
#include "DaqTask.h"
#include "CrcCalculator.h"
#include "bq76pl455adriver.h"
#include "AdcInterface.h"
#include "UpsTask.h"
#include "EmgBrkTask.h"
#include "hibernationTask.h"
#include "InitializeTask.h"
#include "buttons.h"

/*****************************    Defines    *******************************/

#define SW1 	0x10
#define SW2 	0x01

#define BmsBoard		2  // Standard setting
#define CellPerString 	6  // Standard setting
#define TempSense 		3  // Standard setting
#define Cell			BmsBoard*CellPerString  // Standard setting




/*****************************   Constants   *******************************/


/*****************************   Variables   *******************************/


/*****************************   Functions   *******************************/

eReturn_t InitVariables(void)
/*****************************************************************************
*   Input    :  -
*   Output   :  Status of initialization
*   Function :	Initialize global variables
*****************************************************************************/
{
	eReturn_t eVal;
	eVal = eOk;

	// Initialize CellInfo
	CellInfo.NumberOfCell = Cell;
	CellInfo.NumberOfCellPerString = CellPerString;
	CellInfo.NumberOfBmsBoard = BmsBoard;
	CellInfo.NumberOfTempSense = TempSense;
	CellInfo.BatteryCapacity = 2850;

	// Initialize System information
	Voltage[CellInfo.NumberOfCell] = 0;
	Current = 0;
	Temperature[CellInfo.NumberOfCell] = 0;
	StateOfCharge = 0;
	StateOfHealth = 0;
	Fault = 1;

	return eVal;
}


void InitGpio(void)
/*****************************************************************************
*   Input    :  -
*   Output   :  -
*   Function :
*****************************************************************************/
{

	// Init Port A
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
	while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOA)){}
    GPIOPadConfigSet(GPIO_PORTA_BASE, GPIO_PIN_4, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPD);
    GPIODirModeSet(GPIO_PORTA_BASE, GPIO_PIN_4, GPIO_DIR_MODE_OUT);
    GPIOPadConfigSet(GPIO_PORTA_BASE, GPIO_PIN_2, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPD);
    GPIODirModeSet(GPIO_PORTA_BASE, GPIO_PIN_2, GPIO_DIR_MODE_OUT);
    GPIOPadConfigSet(GPIO_PORTA_BASE, GPIO_PIN_3, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPD);
    GPIODirModeSet(GPIO_PORTA_BASE, GPIO_PIN_3, GPIO_DIR_MODE_OUT);
    GPIOPadConfigSet(GPIO_PORTA_BASE, GPIO_PIN_7, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPD);
    GPIODirModeSet(GPIO_PORTA_BASE, GPIO_PIN_7, GPIO_DIR_MODE_OUT);

	// Init Port D
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
	while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOD)){}
    GPIOPadConfigSet(GPIO_PORTD_BASE, GPIO_PIN_2, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPD);
    GPIODirModeSet(GPIO_PORTD_BASE, GPIO_PIN_2, GPIO_DIR_MODE_OUT);

	// Init Port E
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
	while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOE)){}
    GPIOPadConfigSet(GPIO_PORTE_BASE, GPIO_PIN_0, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPD);
    GPIODirModeSet(GPIO_PORTE_BASE, GPIO_PIN_0, GPIO_DIR_MODE_OUT);

	// Set pre setting, to make sure that the state is known. 
	GPIOPinWrite(GPIO_PORTA_BASE,GPIO_PIN_3, 0);
	GPIOPinWrite(GPIO_PORTA_BASE,GPIO_PIN_2, 0x04);

}

eReturn_t InitQueue(void)
/*****************************************************************************
*   Input    :  -
*   Output   :  -
*   Function :	Initialize Queues. 
*****************************************************************************/
{
	eReturn_t eVal;
	uint8_t Thrashcan = 0;
	eVal = eOk;

	UpsEventQueue 				= xQueueCreate( 1, sizeof(uint8_t) );
	UpsStatusEventQueue 		= xQueueCreate( 1, sizeof(uint8_t) );
	ActivateCellBalEventQueue 	= xQueueCreate( 1, sizeof(uint8_t) );
	EnableHibernationEventQueue = xQueueCreate( 1, sizeof(uint8_t) );

	// Make sure that queues are empty. 
	xQueueReceive(EnableHibernationEventQueue,&Thrashcan,0);
	xQueueReceive(UpsStatusEventQueue,&Thrashcan,0);
	xQueueReceive(UpsEventQueue,&Thrashcan,0);


	return eVal;
}

eReturn_t InitSemph(void)
/*****************************************************************************
*   Input    :  -
*   Output   :  -
*   Function :	Initialize Semaphor
*****************************************************************************/
{

	eReturn_t eVal;
	eVal = eOk;
	vSemaphoreCreateBinary(CellInfoSemph);
	vSemaphoreCreateBinary(OpModeSemph);
	// Check if semaphor is created. 
	if( (CellInfoSemph == NULL) | (OpModeSemph == NULL) )
	{
			eVal = eSemphFault;
	}

	return eVal;
}

void setupHardware(void)
/*****************************************************************************
*   Input    :  -
*   Output   :  -
*   Function :	Run all the Init function
*****************************************************************************/
{
  // TODO: Put hardware configuration and initialisation in here
  // Warning: If you do not initialize the hardware clock, the timings will be inaccurate
	 SysCtlClockSet(SYSCTL_USE_PLL|SYSCTL_OSC_MAIN|SYSCTL_XTAL_16MHZ|SYSCTL_SYSDIV_2_5);

 InitGpio();
 ButtonsInit();
 InitStatus = status_led_init();
 InitStatus |= InitVariables();
 InitStatus |= InitUart();
 InitStatus |= InitSysinfo();
 InitStatus |= InitSemph();
 InitStatus |= InitQueue();
 InitStatus |= InitBmsBoard();
 InitStatus |= InitAdc();
 InitStatus |= InitPwm();

}

void Init(void)
/*****************************************************************************
*   Input    :  -
*   Output   :  -
*   Function :	Startup the system, this function determine the cause of startup
*****************************************************************************/
{
	uint32_t ui32Status;
	uint32_t pui32NVData[64];
	uint32_t HibernationData = 0;

//
// Need to enable the hibernation peripheral after wake/reset, before using
// it.
//
	SysCtlPeripheralEnable(SYSCTL_PERIPH_HIBERNATE);
//
// Wait for the Hibernate module to be ready.
//
	while(!SysCtlPeripheralReady(SYSCTL_PERIPH_HIBERNATE))
	{
	}
//
// Determine if the Hibernation module is active.
//
	setupHardware();
	//UARTprintf("Determine Startup...\n");
	if(HibernateIsActive())
	{


	//	CrcCalculator(WakeUp, 3);
	//	TransmitData(5,WakeUp);

		//
		// Read the status to determine cause of wake.
		//
		ui32Status = HibernateIntStatus(false);
		//
		// Test the status bits to see the cause.
		//
		if(ui32Status & HIBERNATE_INT_PIN_WAKE)
		{
	    	OpMode.HibernationMode = FALSE;
	    	OpMode.StandAloneMode = TRUE;
	    	OpMode.UpsMode = FALSE;
			StopSystem();
	    	HibernateDataSet( &HibernationData ,1);
		}else if(ui32Status & HIBERNATE_INT_RTC_MATCH_0)
		{
			//UARTprintf("RTC interrupt \n");
		}else if(ui32Status & HIBERNATE_INT_WR_COMPLETE)
		{
			//UARTprintf("write complete interrupt\n");
		}else if (ui32Status & HIBERNATE_INT_LOW_BAT)
		{
			//UARTprintf("low-battery interrupt\n");
		}else if (ui32Status & HIBERNATE_INT_VDDFAIL)
		{
			//UARTprintf("supply failure interrupt\n");
		}else if (ui32Status & HIBERNATE_INT_RESET_WAKE)
		{
			//UARTprintf("wake from reset pin interrupt\n");
		}else if (ui32Status & HIBERNATE_INT_GPIO_WAKE)
		{
			//UARTprintf("wake from GPIO pin or reset pin interrupt.\n");
		}else
		{
			//UARTprintf("Error - Could not determine wakeup\n");
		}



		//
		// Restore program state information that was saved prior to
		// hibernation.
		//
		HibernateDataGet( &HibernationData ,1); //Which data points needs to be stored ?
		//UARTprintf("Hibernation == %d\n",HibernationData);
	    if ( HibernationData == 1 )
	    {
	    	OpMode.HibernationMode = TRUE;
	    	OpMode.StandAloneMode = FALSE;
	    	OpMode.UpsMode = FALSE;
			StopSystem();
	    }

		//
		// Now that wake up cause has been determined and state has been
		// restored, the program can proceed with normal processor and
		// peripheral initialization.
		//
	}
	//
	// Hibernation module was not active, so this is a cold power-up/reset.
	//
	else
	{
		//UARTprintf("Cold Start \n");
		//
		// Perform normal power-on initialization.
		//
	}
	InitHibernation();

	// transmit all data to the DSP.
	UARTCharPut(UART3_BASE,0xE2);
	UARTCharPut(UART3_BASE,CellInfo.NumberOfCell);
	UARTCharPut(UART3_BASE,CellInfo.NumberOfBmsBoard);
	UARTCharPut(UART3_BASE,CellInfo.NumberOfCellPerString);
	UARTCharPut(UART3_BASE,(uint8_t)(CellInfo.MaximumVoltage >> 8));
	UARTCharPut(UART3_BASE,(uint8_t) (CellInfo.MaximumVoltage));
	UARTCharPut(UART3_BASE,(uint8_t)(CellInfo.MinimumVoltage >> 8));
	UARTCharPut(UART3_BASE,(uint8_t) (CellInfo.MinimumVoltage));
	UARTCharPut(UART3_BASE,CellInfo.NumberOfTempSense);
	UARTCharPut(UART3_BASE,(uint8_t)(CellInfo.BatteryCapacity >> 8));
	UARTCharPut(UART3_BASE,(uint8_t)(CellInfo.BatteryCapacity >> 8));


}

/****************************** End Of Module *******************************/
