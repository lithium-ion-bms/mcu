/*****************************************************************************
* Thesis MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM
*
* MODULENAME.: 
*
* PROJECT....: MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM
*
* DISCRIPTION: These function will enable the possibilty to configure the startup sequence. 
*
*
* Change Log:
*****************************************************************************
* Date    	Id  Change
* 171120	SQ	Module created
* --------------------
*
*****************************************************************************/

#ifndef INITIALIZETASK_H_
  #define INITIALIZETASK_H_

/***************************** Include files *******************************/

/*****************************    Defines    *******************************/

/********************** External declaration of Variables ******************/

/*****************************   Constants   *******************************/

/*************************  Function interfaces ****************************/


void setupHardware(void);
void Init(void);


/****************************** End Of Module *******************************/
#endif
