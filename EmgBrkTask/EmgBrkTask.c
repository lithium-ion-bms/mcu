/*****************************************************************************
* Thesis MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM  
*
* MODULENAME.: EmgBrkTask.c
*
* PROJECT....:  MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
*
* Change Log:
*****************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
* 171002	SQ	Moduel Created
*****************************************************************************/

/***************************** Include files *******************************/
#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "FreeRTOS.h"
#include "Task.h"
#include "queue.h"
#include "semphr.h"
#include "TypeDef.h"
#include "binary.h"
#include "Sysinfo.h"
#include "UARTstdio.h"
#include "hw_memmap.h"
#include "adc.h"
#include "gpio.h"
#include "EmgBrkTask.h"
#include "StopSystem.h"
#include "AdcInterface.h"

/*****************************    Defines    *******************************/

#define EmgBrkFreq1Hz 	1000

/*****************************   Constants   *******************************/

/*****************************   Variables   *******************************/
static uint8_t CurrentFault = 0;
static uint8_t VoltageFault = 0;

/*****************************   Functions   *******************************/
uint8_t CheckCurrent(void);
uint8_t CheckTemperature(void);
uint8_t CheckVoltage(void);
void CurrentISR(void);
void VoltageISR(void);

void CurrentISR(void)
/*****************************************************************************
*   Input    : -
*   Output   : -
*   Function : Evaluate Over Current interrupt. 
******************************************************************************/
{
	Fault = 3;
	CurrentFault = Fault;
	StopSystem();
	ADCComparatorIntClear(ADC0_BASE,0xb);
	ADCSequenceStepConfigure(ADC0_BASE, 0, 0, ADC_CTL_END | ADC_CTL_CH1 );
	ADCSequenceEnable(ADC0_BASE, 0);
}

uint8_t CheckCurrent(void)
/*****************************************************************************
*   Input    : -
*   Output   : -
*   Function : Validate Current Fault Handlng
******************************************************************************/
{
	uint16_t AdcValue = 0;
	uint8_t Return = 1;
	static uint8_t CurrentFault;

	AdcValue = AdcGet(ADC0_BASE, 0); 
	if ( (AdcValue > 0xEEE ) && (Charge == TRUE))
	{
		StopSystem();
		Return = 3;
	}else if ( (AdcValue > 0xEEE) && (Discharge == TRUE))
	{
		StopSystem();
		Return = 3;
	}else if ( (Fault != 3) | (CurrentFault == 3) )
	{
		Return = 1;
	}
	CurrentFault = Return;
	return Return;
}

uint8_t CheckTemperature(void)
/*****************************************************************************
*   Input    : -
*   Output   : -
*   Function : Validate Temperature Fault Handlng
******************************************************************************/
{
	uint8_t Temp = 0;
	static uint8_t PrevTemp[16];
	uint8_t Return = 1;
	static uint8_t TempFault = 0;

	for ( int i = 0; i < CellInfo.NumberOfTempSense*CellInfo.NumberOfBmsBoard; i++)
	{
		Temp = Temperature[i];

		if ( (Temp > 50) )
		{
			Return = 2;
			if ( (Return != 3) && ((Temp > 60) || (PrevTemp[i] <= Temp )))
			{
				Return = 3;
				StopSystem();
			}
		}else if ( (Return != 3) && (( Temp < 10 ) ))
		{
			Return = 2;
			if ( (Temp <= 0 ) || ((PrevTemp[i] >= Temp) ))
			{
				Return = 3;
				StopSystem();
			}
		}else if ( (Return != 3) && (StateOfHealth < 50 ))
		{
			Return = 2;
			if ( StateOfHealth < 25 )
			{
				Return = 3;
				StopSystem();
			}
		}else if ( (Return != 3) && ((Fault != 3) || (TempFault == 3)) )
		{
			Return = 1;
		}
		PrevTemp[i] = Temp;
	}
	TempFault = Return;
	return Return;
}

uint8_t CheckVoltage(void)
/*****************************************************************************
*   Input    : -
*   Output   : -
*   Function : Validate Voltage Fault Handlng
******************************************************************************/
{
	uint16_t AdcValue = 0;
	uint8_t Return = 1;
	AdcValue = AdcGet(ADC1_BASE, 0);

	if ( (Fault != 3) | ((VoltageFault == 3) )  )
	{
		Return = 1;
	}

	for ( uint8_t i = 0; i < CellInfo.NumberOfCell; i++ )
	{
		if ( Voltage[i] >> 5 == 0 ) 
		{
			Return = 2;
		}

		if ( (Voltage[i] < 2600) && ((Voltage[i] >> 5) > 0) ) 
		{
			Return = 3;
			StopSystem();
		}

		if ( Voltage[i] > 3600 ) 
		{
			Return = 3;
			StopSystem();
		}
	}

	VoltageFault = Return;
	return Return;
}

void EmgBrk_Task(void *pvParameters)
/*****************************************************************************
*   Input    : -
*   Output   : -
*   Function : Fault handling task. Check for any critical event. 
******************************************************************************/
{
	uint8_t ReturnCurrent = 1;
	uint8_t ReturnTemperature = 1;
	uint8_t ReturnVoltage = 1;

	while(1)
	{
		ReturnCurrent 		= CheckCurrent();
		ReturnVoltage 		= CheckVoltage();
		ReturnTemperature 	= CheckTemperature();
		if ( (ReturnCurrent == 3) || (ReturnTemperature == 3) || (ReturnVoltage == 3))
		{
			Fault = 3;
		}else if ( (ReturnCurrent == 2) || (ReturnTemperature == 2) || (ReturnVoltage == 2))
		{
			Fault = 2;
		}else if ( (ReturnCurrent == 1) || (ReturnTemperature == 1) || (ReturnVoltage == 1))
		{
			Fault = 1;
		}
		vTaskDelay(EmgBrkFreq1Hz / portTICK_RATE_MS); // wait 100 ms.
	}
}
/****************************** End Of Module *******************************/
