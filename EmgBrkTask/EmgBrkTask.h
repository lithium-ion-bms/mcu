/*****************************************************************************
* Thesis MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM  
*
* MODULENAME.: EmgBrkTask.c
*
* PROJECT....: MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
* DISCRIPTION: This task will control the fault handling of the system. 
*
* Change Log:
*****************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
* 171002	SQ	Moduel Created
*****************************************************************************/

#ifndef HeaderName_H_
  #define HeaderName_H_

/***************************** Include files *******************************/

/*****************************    Defines    *******************************/

/********************** External declaration of Variables ******************/

/*****************************   Constants   *******************************/

/*************************  Function interfaces ****************************/

void EmgBrk_Task(void *pvParameters);
void CurrentISR(void);


/****************************** End Of Module *******************************/
#endif
