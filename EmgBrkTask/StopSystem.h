/*****************************************************************************
* Thesis MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM  
*
* MODULENAME.: EmgBrkTask.c
*
* PROJECT....: MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
* DISCRIPTION: API function for controllling the contactor. 
*
*
* Change Log:
*****************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
* 171002	SQ	Module Created
*****************************************************************************/

#ifndef StopSystem_H_
  #define StopSystem_H_

/***************************** Include files *******************************/

/*****************************    Defines    *******************************/

/********************** External declaration of Variables ******************/

/*****************************   Constants   *******************************/

/*************************  Function interfaces ****************************/

void StopSystem(void);
void StartSystem(void);


/****************************** End Of Module *******************************/
#endif
