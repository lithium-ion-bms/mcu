/*****************************************************************************
* Thesis MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM  
*
* MODULENAME.: EmgBrkTask.c
*
* PROJECT....:  MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
*
* Change Log:
*****************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
* 171002	SQ	Module Created
*****************************************************************************/

/***************************** Include files *******************************/
#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "FreeRTOS.h"
#include "Task.h"
#include "queue.h"
#include "semphr.h"
#include "TypeDef.h"
#include "binary.h"
#include "Sysinfo.h"
#include "gpio.h"
#include "hw_memmap.h"


/*****************************    Defines    *******************************/

/*****************************   Constants   *******************************/

/*****************************   Variables   *******************************/

/*****************************   Functions   *******************************/


void StopSystem(void)
/*****************************************************************************
*   Input    : - 
*   Output   : - 
*   Function : Disconnect the contactor
******************************************************************************/
{
	GPIOPinWrite(GPIO_PORTA_BASE,GPIO_PIN_4,(0x00));
}

void StartSystem(void)
/*****************************************************************************
*   Input    : -
*   Output   : -
*   Function : Connect the contactor
******************************************************************************/
{
	GPIOPinWrite(GPIO_PORTA_BASE,GPIO_PIN_4,(0x10));
}

/****************************** End Of Module *******************************/
