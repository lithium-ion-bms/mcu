/*****************************************************************************
* Thesis Lithium-ion modular battery 
*
* MODULENAME.: CrcCalculator
*
* PROJECT....:  Lithium Ion Battery Management System
*
*
* Change Log:
*****************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
* 171004 SQ 	Module created
*****************************************************************************/

#ifndef CRCCALCULATOR_H_
  #define CRCCALCULATOR_H_

/***************************** Include files *******************************/

/*****************************    Defines    *******************************/

/********************** External declaration of Variables ******************/

/*****************************   Constants   *******************************/

/*************************  Function interfaces ****************************/

void CrcCalculator(uint8_t *Data, uint8_t Siz);
bool_t CrcCheck(uint8_t *pData, uint8_t Size);


/****************************** End Of Module *******************************/
#endif
