/*****************************************************************************
* Thesis Lithium-ion modular battery 
*
* MODULENAME.: CrcCalculator
*
* PROJECT....:  Lithium Ion Battery Management System
*
*
* Change Log:
*****************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
* 171004 SQ 	Module created
* 171012 SQ 	Added CrcCalculate() and CrcCheck()
*****************************************************************************/

/***************************** Include files *******************************/
#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "FreeRTOS.h"
#include "Task.h"
#include "queue.h"
#include "semphr.h"
#include "TypeDef.h"
#include "binary.h"
#include "Sysinfo.h"

#include "CrcCalculator.h"

/*****************************    Defines    *******************************/

/*****************************   Constants   *******************************/

/*****************************   Variables   *******************************/

/*****************************   Functions   *******************************/
/*****************************   Prototypes   *******************************/

uint16_t Crc(uint8_t Data, uint16_t PrevCrc);


uint16_t Crc(uint8_t Data, uint16_t PrevCrc)
/*****************************************************************************
*   Input    : Data & result from previous byte calculation
*   Output   : CRC for the given byte, included previous calculation
*   Function : Calculate the CRC16-IBM for a given set of bytes.
*   		   If Data is multiple of bytes, then the algorithm calculate
*   		   one byte at a time, where PrevCrc are the CRC for the previous
*   		   byte.
******************************************************************************/
{
	uint8_t Leftshift = 0; // Count Leftshift
	uint64_t Crc  = 0; // Crc
	uint32_t Pol = 0x18005; // x_16+x_15+x_2+1
	uint8_t ReverseTable[16] =
	{
		0x0, 0x8, 0x4, 0xc, 0x2, 0xa, 0x6, 0xe, 0x1, 0x9, 0x5, 0xd, 0x3, 0xb, 0x7, 0xf
	};

	Data = (ReverseTable[(Data & 0xf)] << 4) | (ReverseTable[(Data >> 4)]);// Reverse the data and previous CRC for multiple of data bytes
	PrevCrc = (ReverseTable[((PrevCrc & 0x0f00) >> 8)] << 12) | (ReverseTable[((PrevCrc & 0xf000) >> 12)] << 8) | (ReverseTable[((PrevCrc & 0x000f))] << 4) | ReverseTable[((PrevCrc & 0x00f0) >> 4)]; // 16bit

	Crc  = Data <<  8; // Lefthift data 8 times, to make sure that 16 bit are available to the remainder.

	Crc = (Crc ) ^ (PrevCrc); // XOR the previous CRC with the new data.
	while (Leftshift < 9)
	{
		if ((Crc  & ((0x1 << 16)))) // If Crc's LSb in the third byte is set, then XOR with polynomial
		{
			Crc  = Crc  ^ Pol;
		} else if (Leftshift < 8) // else leftshift 1
		{
			Crc  = Crc  << 1;
			Leftshift++;
		} else Leftshift++;     // when Lefshifted 8 times and Crc's LSb in the third byte was set.
	}

		return (ReverseTable[((Crc & 0x0f00) >> 8)] << 12) | (ReverseTable[((Crc  & 0xf000) >> 12)] << 8) | (ReverseTable[((Crc  & 0x000f))] << 4) | ReverseTable[((Crc  & 0x00f0) >> 4)];
}


void CrcCalculator(uint8_t *Data, uint8_t Size)
/*****************************************************************************
*   Input    : Data Array and size of data
*   Output   : CrC in the Data Array
*   Function : Calculate the CRC16-IBM for a given set of bytes.
*   		   If Data is multiple of bytes, then the algorithm calculate
*   		   one byte at a time, where PrevCrc are the CRC for the previous
*   		   byte.
******************************************************************************/
{
	uint16_t CRC = 0;
	for (int i = 0; i < Size; i++)
	{
		CRC = Crc(Data[i], CRC);
	}
	Data[Size] = (uint8_t) (CRC >> 8);
	Data[Size+1] = (uint8_t) CRC;
}

bool_t CrcCheck(uint8_t *pData, uint8_t Size)
/*****************************************************************************
*   Input    : Data Array and size of data
*   Output   : Returns false if CRC is != 0 and true if CRC is = 0
*   Function : Checks the CRC from the received data,
******************************************************************************/
{
	bool_t Return = TRUE;
	CrcCalculator(pData,Size); // Check the CRC bytes.
	if ( ( pData[Size] != 00 ) | ( pData[Size+1] != 00 ))
	{
			Return = FALSE;
	}
	pData[Size-1] = pData[Size+1];
	pData[Size-2] = pData[Size];
	return Return;
}

/****************************** End Of Module *******************************/
