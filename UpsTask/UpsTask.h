/*****************************************************************************
* Thesis MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM  
*
* MODULENAME.: UpsTask
*
* PROJECT....: MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
* DISCRIPTION: Communicate with the UPS, in this case the matlab script.
*
*
* Change Log:
*****************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
* 171017 SQ 	Module created
*****************************************************************************/

#ifndef UPSTASK_H_
  #define UPSTASK_H_

/***************************** Include files *******************************/

/*****************************    Defines    *******************************/

/********************** External declaration of Variables ******************/

/*****************************   Constants   *******************************/

/*************************  Function interfaces ****************************/

void Ups_Task(void *pvParameters);


/****************************** End Of Module *******************************/
#endif
