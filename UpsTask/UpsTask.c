/*****************************************************************************
* Thesis MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
* MODULENAME.: UpsTask
*
* PROJECT....:  MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
*
* Change Log:
*****************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
* 171017  SQ 	Module created
*****************************************************************************/

/***************************** Include files *******************************/
#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "FreeRTOS.h"
#include "Task.h"
#include "queue.h"
#include "semphr.h"
#include "TypeDef.h"
#include "binary.h"
#include "Sysinfo.h"
#include "uart.h"
#include "hw_memmap.h"
#include "CRCCalculator.h"
#include "bq76PL455ADriver.h"

/*****************************    Defines    *******************************/
#define UpsFreq2Hz		500
#define Opmode			0x00
#define Data 			0x10
#define CellInformation	0x20
#define SystemSoc		0x30
#define CmdMask			0x70
#define OpModeMask		0x03
#define Hibernation		0x00
#define Standalone		0x01
#define Ups				0x02
#define ReadWriteMask	0x80
#define Read			0x80
#define Write			0x00
#define MiliVoltScale 	100		// Matlab transmit in deci Volt.

/*****************************   Constants   *******************************/

/*****************************   Variables   *******************************/

/*****************************   Functions   *******************************/
void Ups_Task(void *pvParameters);
void UpdateOpMode(uint8_t *pData);
void UpdateUpsEvent(uint8_t *pData);
void UpsDataHandler(uint8_t *pData);
void UpdateCellInformation(uint8_t *pData);


void UpdateCellInformation(uint8_t *pData)
/*****************************************************************************
*   Input    : Cellinfo command and the new information
*   Output   : -
*   Function : Update cellinfo
******************************************************************************/
{
	// Get the new cellinfo and store it in global variables. 
	if ( CellInfo.NumberOfCellPerString != pData[1] )
	{
		ChangeNumberOfCell(pData[1]);
		CellInfo.NumberOfCellPerString 	= pData[1];
	}
	if ( CellInfo.NumberOfBmsBoard != pData[2] )
	{
		ChangeNumberOfBmsBoard(pData[2]);
		CellInfo.NumberOfBmsBoard 		= pData[2];
	}
	CellInfo.MaximumVoltage 		= pData[3]*MiliVoltScale;
	CellInfo.MinimumVoltage 		= pData[4]*MiliVoltScale;
	if ( CellInfo.NumberOfTempSense != pData[5] )
	{
		ChangeNumberOfTemperatureSense(pData[5]);
		CellInfo.NumberOfTempSense 		= pData[5];
	}
	CellInfo.NumberOfCell		 	= pData[1] * pData[2];

	// Transmit new information to the DSP
	UARTCharPut(UART3_BASE,0xE2);
	UARTCharPut(UART3_BASE,CellInfo.NumberOfCell);
	UARTCharPut(UART3_BASE,CellInfo.NumberOfBmsBoard);
	UARTCharPut(UART3_BASE,CellInfo.NumberOfCellPerString);
	UARTCharPut(UART3_BASE,(uint8_t)(CellInfo.MaximumVoltage >> 8));
	UARTCharPut(UART3_BASE,(uint8_t) (CellInfo.MaximumVoltage));
	UARTCharPut(UART3_BASE,(uint8_t)(CellInfo.MinimumVoltage >> 8));
	UARTCharPut(UART3_BASE,(uint8_t) (CellInfo.MinimumVoltage));
	UARTCharPut(UART3_BASE,CellInfo.NumberOfTempSense);
	UARTCharPut(UART3_BASE,(uint8_t)(CellInfo.BatteryCapacity >> 8));
	UARTCharPut(UART3_BASE,(uint8_t)(CellInfo.BatteryCapacity >> 8));


}


void UpdateOpMode(uint8_t *pData)
/*****************************************************************************
*   Input    : OpMode command, and the new opmode. 
*   Output   : -
*   Function : Change the opmode, command from UPS (MATLAB)
******************************************************************************/
{
	// Init variables
	uint8_t SendBackOpMode[4] = {0x01, 0xff, 0xff, 0xff};
	// Check Semaphor
	xSemaphoreTake(OpModeSemph,UpsFreq2Hz / portTICK_RATE_MS);
	// Determine the OpMode and change it. 
	if ( ( pData[0] & ReadWriteMask ) == 0x00)
	{
		switch(pData[1] & OpModeMask )
		{
		case Hibernation:
			OpMode.HibernationMode = TRUE;
			OpMode.StandAloneMode = FALSE;
			OpMode.UpsMode = FALSE;
			StopSystem();
			UARTCharPut(UART3_BASE,0x01);
			break;
		case Standalone:
			OpMode.HibernationMode = FALSE;
			OpMode.StandAloneMode = TRUE;
			OpMode.UpsMode = FALSE;
			StopSystem();
			break;
		case Ups:
			OpMode.HibernationMode = FALSE;
			OpMode.StandAloneMode = FALSE;
			OpMode.UpsMode = TRUE;
			StartSystem();
			break;
		default:
			break;
		}
	}else
	{
		if ( (OpMode.HibernationMode == TRUE)  )
		{
			SendBackOpMode[1] = Hibernation;
		} else if ( OpMode.StandAloneMode == TRUE )
		{
			SendBackOpMode[1] = Standalone;
		} else
		{
			SendBackOpMode[1] = Ups;
		}
		for (int i = 0; i < 2; i++ )
		{
			UARTCharPut(UART0_BASE, SendBackOpMode[i]);
		}
	}
	xSemaphoreGive(OpModeSemph);
}




void UpdateUpsEvent(uint8_t *pData)
/*****************************************************************************
*   Input    : Command and/or data. 
*   Output   : - 
*   Function : Determine the UpsEvent and activate the queue. 
******************************************************************************/
{
	uint8_t pRead = Read;
	switch(pData[0] & ReadWriteMask )
	{
	case Read:
		// Request data
		xQueueSendToBack(UpsEventQueue,&pRead,portMAX_DELAY);
		break;
	case Write:
		// SystemSoc
		xQueueSendToBack(UpsEventQueue,&pData[1],portMAX_DELAY);
		break;
	default:
		break;
	}
}


void UpsDataHandler(uint8_t *pData)
/*****************************************************************************
*   Input    : Command
*   Output   : -
*   Function : Determine the command and call the function to execute. 
******************************************************************************/
{
	uint8_t Com = pData[0];
	switch( Com & CmdMask)
	{
		case Opmode:
			UpdateOpMode(pData);
			break;
		case Data:
			UpdateUpsEvent(pData);
			break;
		case CellInformation:
			UpdateCellInformation(pData);
			break;
		case SystemSoc:
			UpdateUpsEvent(pData);
			break;
		default:
			break;
	}

}


void Ups_Task(void *pvParameters)
/*****************************************************************************
*   Input    : -
*   Output   : -
*   Function : Task to handle the request and transmit UpsEvent/data
******************************************************************************/
{
	//Init variables
	
	uint8_t Count = 0;
	uint8_t UARTData[10];
	
	while(1)
	{	
		// Check UART, if no data from the MATLAB block task. 
		while(UARTCharsAvail(UART0_BASE) == FALSE)
		{
			vTaskDelay(10 / portTICK_RATE_MS);
		};
		// Get all data. 
		while(UARTCharsAvail(UART0_BASE) == TRUE )
		{
			UARTData[Count] = UARTCharGet(UART0_BASE);
			Count++;
			vTaskDelay(1 / portTICK_RATE_MS);
		}

		if ( Count != 0 )
		{
			UpsDataHandler(UARTData);
		}

		// Cleanup. 
		while (Count)
		{
			UARTData[Count] = 0;
			Count--;
		}

	}

}
/****************************** End Of Module *******************************/
