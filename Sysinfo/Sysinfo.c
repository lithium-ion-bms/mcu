/*****************************************************************************
* Thesis MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
* MODULENAME.: Sysinfo.c
*
* PROJECT....: MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
*
* Change Log:
*****************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
* 170730  SQ    Module Created
*****************************************************************************/

/***************************** Include files *******************************/
#include "Sysinfo.h"
#include <stdint.h>
#include "pin_map.h"
#include "tm4c123gh6pm.h"
#include "TypeDef.h"
#include "FreeRTOS.h"
#include "Task.h"
#include "queue.h"
#include "semphr.h"
#include "systick.h"
#include "tmodel.h"
#include "sysctl.h"
#include "hw_memmap.h"
#include "uart.h"
#include "uartstdio.h"


/*****************************    Defines    *******************************/

/*****************************   Constants   *******************************/


/*****************************   Variables   *******************************/

/*****************************   Functions   *******************************/



eReturn_t InitSysinfo(void)
/*****************************************************************************
*   Input    : -
*   Output   : -
*   Function : Init System information
******************************************************************************/
{
	eReturn_t eVal;
	eVal = eOk;

	for ( uint8_t i = 0; (i < CellInfo.NumberOfCell); i++ ) {
		Voltage[i] = 0;
		Current = 0;
		Temperature[i] = 0;
		StateOfCharge = 0;
		StateOfHealth = 0;
	}

	OpMode.HibernationMode = FALSE;
	OpMode.StandAloneMode = TRUE;
	OpMode.UpsMode = FALSE;
	StopSystem();

	Discharge = TRUE;
	Charge = TRUE;

	return eVal;
}

/****************************** End Of Module *******************************/
