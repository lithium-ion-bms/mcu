/*****************************************************************************
* Thesis MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
* MODULENAME.: Sysinfo
*
* PROJECT....: MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
* DISCRIPTION: This module contain all the global variables, with associated
* 				set and get function.
*
*
* Change Log:
*****************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
* 170730  SQ    Module Created
*****************************************************************************/

#ifndef Sysinfo_H_
  #define Sysinfo_H_

/***************************** Include files *******************************/
#include "TypeDef.h"
#include "FreeRTOS.h"
#include "semphr.h"
/*****************************    Defines    *******************************/
#define MaxNumberOfBmsBoard		6
#define MaxNumberOfCell			6*MaxNumberOfBmsBoard
/********************** External declaration of Variables ******************/


// Struct containing information the number of cells in the physical setup.
typedef struct{
	uint8_t NumberOfCell;
	uint8_t NumberOfBmsBoard;
	uint8_t NumberOfCellPerString;
	uint16_t MaximumVoltage; // Measured in mV
	uint16_t MinimumVoltage;
	uint8_t NumberOfTempSense;
	uint16_t BatteryCapacity;
} ui8CellNumber_t;

// Struct for defining the different operation mode of the battery system.
typedef struct {
	bool_t HibernationMode;
	bool_t UpsMode;
	bool_t StandAloneMode;
} bOpMode_t;





/*****************************   Constants   *******************************/

// Initialize Queue and Semaphor

eReturn_t InitStatus;

xSemaphoreHandle CellInfoSemph;
xSemaphoreHandle OpModeSemph;
xQueueHandle UpsEventQueue;
xQueueHandle UpsStatusEventQueue;
xQueueHandle ActivateCellBalEventQueue;
xQueueHandle EnableHibernationEventQueue;






/*****************************   Variables   *******************************/


ui8CellNumber_t CellInfo;
bOpMode_t OpMode;

int16_t Voltage[16];
uint16_t Current;
int8_t Temperature[16];
uint8_t StateOfCharge;
uint8_t StateOfHealth;
uint16_t SystemVoltage;
uint8_t Fault;
bool_t Discharge;
bool_t Charge;


/*************************  Function interfaces ****************************/

eReturn_t InitSysinfo(void);

/****************************** End Of Module *******************************/
#endif
