
/*****************************************************************************
* Thesis MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
* MODULENAME.: main.c
*
* PROJECT....: MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
* DESCRIPTION: See module specification file (.h-file).
*
* Change Log:
******************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
* 170901  SQ    Module created.
*
*****************************************************************************/

/***************************** Include files *******************************/

#include <stdint.h>
#include "pin_map.h"
#include "tm4c123gh6pm.h"
#include "TypeDef.h"
#include "FreeRTOS.h"
#include "Task.h"
#include "queue.h"
#include "semphr.h"
#include "systick.h"
#include "tmodel.h"
#include "sysctl.h"
#include "hw_memmap.h"
#include "uart.h"
#include "uartstdio.h"
#include "rom.h"
#include "gpio.h"
#include "adc.h"

#include "Sysinfo.h"
#include "status_led.h"
#include "InfoTask.h"
#include "DaqTask.h"
#include "CrcCalculator.h"
#include "bq76pl455adriver.h"
#include "AdcInterface.h"
#include "UpsTask.h"
#include "EmgBrkTask.h"
#include "hibernationTask.h"
#include "InitializeTask.h"
#include "CellBalancingTask.h"
#include "controlTask.h"

/*****************************    Defines    *******************************/

#define TARGET_IS_TM4C123_RA1
#define USERTASK_STACK_SIZE configMINIMAL_STACK_SIZE
#define IDLE_PRIO 0
#define LOW_PRIO  1
#define MED_PRIO  2
#define HIGH_PRIO 3

/*****************************   Constants   *******************************/

/*****************************   Variables   *******************************/

/*****************************   Functions   *******************************/

int main(void)
/*****************************************************************************
*   Input    :  -
*   Output   :  -
*   Function :
*****************************************************************************/
{
	//setupHardware();
	  Init();
  /*
   Start the tasks defined within this file/specific to this demo.
   */

  xTaskCreate( status_led_task, ( signed portCHAR * ) "Status_led", USERTASK_STACK_SIZE, NULL, LOW_PRIO, NULL );
  xTaskCreate( Daq_task, 		( signed portCHAR * ) "Daq", 		USERTASK_STACK_SIZE, NULL, MED_PRIO, NULL );
   xTaskCreate( Ups_Task, 		( signed portCHAR * ) "Ups", 		USERTASK_STACK_SIZE, NULL, LOW_PRIO, NULL );
    xTaskCreate( Info_task, 		( signed portCHAR * ) "Info",	 	USERTASK_STACK_SIZE, NULL, LOW_PRIO, NULL );
  xTaskCreate( EmgBrk_Task, 	( signed portCHAR * ) "EmgBrk", 	USERTASK_STACK_SIZE, NULL, LOW_PRIO, NULL );
  xTaskCreate( Hibernation_task,( signed portCHAR * ) "Hibernation",USERTASK_STACK_SIZE, NULL, LOW_PRIO, NULL );
//  xTaskCreate( CellBal_Task, 	( signed portCHAR * ) "CellBal", 	USERTASK_STACK_SIZE, NULL, LOW_PRIO, NULL );
  xTaskCreate( Control_Task, 	( signed portCHAR * ) "Control", 	USERTASK_STACK_SIZE, NULL, LOW_PRIO, NULL );

  /*
   * Start the scheduler.
   */
  vTaskStartScheduler();

  /*
   * Will only get here if there was insufficient memory to create the idle task.
   */
	GPIOPinWrite(GPIO_PORTF_BASE,(GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 |GPIO_PIN_3 ),(0x02));
  return 1;
}
