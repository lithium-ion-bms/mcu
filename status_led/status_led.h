/*****************************************************************************
* Thesis MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
* MODULENAME.: leds.c
*
* PROJECT....: MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
* DESCRIPTION: I'am alive status LED. 
*
* Change Log:
******************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
* 170901  SQ    Module created.
*
*****************************************************************************/

#ifndef _STATUS_LED_H
  #define _STATUS_LED_H

/***************************** Include files *******************************/

/*****************************    Defines    *******************************/

/*****************************   Constants   *******************************/

/*****************************   Functions   *******************************/


eReturn_t status_led_init(void);

void status_led_task(void *pvParameters);


/****************************** End Of Module *******************************/
#endif

