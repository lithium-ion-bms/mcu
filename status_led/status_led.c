/*****************************************************************************
* Thesis MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
* MODULENAME.: leds.c
*
* PROJECT....: MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
* DESCRIPTION: See module specification file (.h-file).
*
* Change Log:
******************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
* 170901  SQ    Module created.
*
*****************************************************************************/

/***************************** Include files *******************************/
#include <stdint.h>
#include "pin_map.h"
#include "FreeRTOS.h"
#include "Task.h"
#include "queue.h"
#include "semphr.h"
#include "TypeDef.h"
#include "binary.h"
#include "status_led.h"
#include "sysinfo.h"
#include "gpio.h"
#include "hw_memmap.h"
#include "sysctl.h"

/*****************************    Defines    *******************************/

#define Red    GPIO_PIN_1
#define Green  GPIO_PIN_3
#define Yellow GPIO_PIN_3 | GPIO_PIN_1
/*****************************   Constants   *******************************/

/*****************************   Variables   *******************************/

/*****************************   Functions   *******************************/

eReturn_t status_led_init(void)
/*****************************************************************************
*   Input    : 	-
*   Output   : 	-
*   Function : 	
*****************************************************************************/
{
	eReturn_t eVal;
	uint8_t timeout = 0;

	//
	// Enable the GPIOA peripheral
	//
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
	//
	// Wait for the GPIOA module to be ready.
	//
	while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOF))
	{
		if ( timeout == 10)
		{
			return eCellNumberFailed;
		}
		timeout++;
	}
	eVal = eOk;
	//
	// Initialize the GPIO pin configuration.
	//
	//
	// Set pins 0 and 3 as output, SW controlled.
	//
	GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_3 | GPIO_PIN_2 | GPIO_PIN_1 );//| GPIO_PIN_0);
	//
	// Enable the pin interrupts.
	//
  return eVal;
}


void status_led_task(void *pvParameters)
{

 uint8_t Pass = Green;
 uint8_t Failed = Red;
 uint8_t Hibernation = Yellow;


	while(1)
	{


		if ( InitStatus != eOk )
		{
			GPIOPinWrite(GPIO_PORTF_BASE,( GPIO_PIN_1 | GPIO_PIN_2 |GPIO_PIN_3 ),(Failed));
			Failed ^= Red;
			//I2cTransmit(Pass);
		}else if ( OpMode.HibernationMode == TRUE )
		{
			GPIOPinWrite(GPIO_PORTF_BASE,( GPIO_PIN_1 | GPIO_PIN_2 |GPIO_PIN_3 ),(Hibernation));
			Hibernation ^= Yellow;
		}else
		{
		// Toggle status led
			GPIOPinWrite(GPIO_PORTF_BASE,( GPIO_PIN_1 | GPIO_PIN_2 |GPIO_PIN_3 ),(Pass));
			Pass ^= Green;
		//	I2cTransmit(0x20);123: GPIOPinWrite(GPIO_PORTE_BASE,GPIO_PIN_0, GPIO_PIN_0);
		}
		vTaskDelay(500 / portTICK_RATE_MS); // wait 100 ms.

	}
}


/****************************** End Of Module *******************************/




