/*****************************************************************************
* Thesis MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
* MODULENAME.: CellBalancingTasl.c
*
* PROJECT....:  MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
*
* Change Log:
*****************************************************************************
* Date    	Id  Change
* YYMMDD
* --------------------
* 171113	SQ	Module created
*****************************************************************************/

/***************************** Include files *******************************/
#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "FreeRTOS.h"
#include "Task.h"
#include "queue.h"
#include "semphr.h"
#include "TypeDef.h"
#include "binary.h"
#include "Sysinfo.h"

/*****************************    Defines    *******************************/

/*****************************   Constants   *******************************/

/*****************************   Variables   *******************************/

/*****************************   Functions   *******************************/


void CellBal_Task(void *pvParameters)
/*****************************************************************************
*   Input    : -
*   Output   : -
*   Function : Task to handle cell balancing
******************************************************************************/
{
	uint8_t Activate = 0;

	while (1)
	{
		// wait for activation signal from charging. 
		xQueuePeek(ActivateCellBalEventQueue,Activate,portMAX_DELAY);
		uint8_t MaxCellNumber = 0;
		uint16_t MaxCellVoltage = 0;
		uint8_t BmsBoard = 0;
		uint16_t Division = CellInfo.NumberOfCell;

		// Check which cell to balance
		for ( int i = 0; i < CellInfo.NumberOfCell; i++ )
		{
			if ( Voltage[i] > MaxCellVoltage )
			{
				MaxCellVoltage = Voltage[i];
				MaxCellNumber = i;
			}
		}
		Division = MaxCellNumber;

		// Determine the BMS board
		while ( CellInfo.NumberOfCellPerString < Division )
		{
			Division = Division/CellInfo.NumberOfBmsBoard;
			BmsBoard++;
		}
		// Request balancing on that cell
		CellBalCmd(MaxCellNumber,BmsBoard);
		vTaskDelay(100 / portTICK_RATE_MS); // wait 100 ms.

	}

}

/****************************** End Of Module *******************************/
