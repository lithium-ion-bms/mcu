/*****************************************************************************
* Thesis MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 

* MODULENAME.: CellBalancingTasl.c
*
* PROJECT....: MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
* DISCRIPTION:
*
*
* Change Log:
*****************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
* 171113	SQ	Module created
*****************************************************************************/

#ifndef CELLBALANCINGTASK_H_
  #define CELLBALANCINGTASK_H_

/***************************** Include files *******************************/

/*****************************    Defines    *******************************/

/********************** External declaration of Variables ******************/

/*****************************   Constants   *******************************/

/*************************  Function interfaces ****************************/

void CellBal_Task(void *pvParameters);


/****************************** End Of Module *******************************/
#endif
