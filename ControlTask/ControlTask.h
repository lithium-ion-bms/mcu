/*****************************************************************************
* Thesis MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM  
*
* MODULENAME.: ControlTask.h
*
* PROJECT....: MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
* DISCRIPTION: This task Controls the periphirals, this could be to enable/Disable 
				charging/discharging and contactor control. 
*
*
* Change Log:
*****************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
* 171120	SQ 	Module created
*****************************************************************************/

#ifndef ControlTask_H_
  #define ControlTask_H_

/***************************** Include files *******************************/

/*****************************    Defines    *******************************/

/********************** External declaration of Variables ******************/

/*****************************   Constants   *******************************/

/*************************  Function interfaces ****************************/

void Control_Task(void *pvParameters);


/****************************** End Of Module *******************************/
#endif
