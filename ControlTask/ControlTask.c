/*****************************************************************************
* Thesis MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
* MODULENAME.: ControlTask.h
*
* PROJECT....:  MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
*
* Change Log:
*****************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
* 171120	SQ 	Module created
*****************************************************************************/

/***************************** Include files *******************************/
#include <stdint.h>
#include <stdbool.h>
#include "tm4c123gh6pm.h"
#include "FreeRTOS.h"
#include "Task.h"
#include "queue.h"
#include "semphr.h"
#include "TypeDef.h"
#include "binary.h"
#include "Sysinfo.h"
#include "stopSystem.h"
#include "gpio.h"
#include "hw_memmap.h"
#include "pwm.h"
#include "sysctl.h"
#include "pin_map.h"

/*****************************    Defines    *******************************/
#define SoC 0x7f
#define Connect 0x4
#define Mode 	0x80

/*****************************   Constants   *******************************/

/*****************************   Variables   *******************************/

/*****************************   Functions   *******************************/

void BeginCharge()
/*****************************************************************************
*   Input    : -
*   Output   : -
*   Function :  Activate charging
******************************************************************************/
{
	GPIOPinWrite(GPIO_PORTD_BASE,GPIO_PIN_2, 0x04);
    PWMPulseWidthSet(PWM0_BASE, PWM_OUT_1, 2500);
    Charge = TRUE;
    Discharge = FALSE;
}

void BeginDisCharge(uint8_t Difference)
/*****************************************************************************
*   Input    : The difference between the systemSoc and the ModuleSoC
*   Output   : -
*   Function : Activate dischargin
******************************************************************************/
{
	GPIOPinWrite(GPIO_PORTD_BASE,GPIO_PIN_2, 0x00);
    PWMPulseWidthSet(PWM0_BASE, PWM_OUT_1, 2500/100*Difference);
    Charge = FALSE;
    Discharge = TRUE;
}


void RequestUpsMode()
/*****************************************************************************
*   Input    : -
*   Output   : -
*   Function : Set OpMode and disconnect contactor. 
******************************************************************************/
{
	GPIOPinWrite(GPIO_PORTD_BASE,GPIO_PIN_2, 0x00);
    PWMPulseWidthSet(PWM0_BASE, PWM_OUT_1, 2500);

    if ( (SystemVoltage/1000)*1000 == 22000)
    {
    	OpMode.HibernationMode = FALSE;
    	OpMode.StandAloneMode = FALSE;
    	OpMode.UpsMode = TRUE;
    	StartSystem();
    }

    Charge = FALSE;
    Discharge = TRUE;
}


eReturn_t InitPwm()
/*****************************************************************************
*   Input    : -
*   Output   : -
*   Function : Init PWM for discharger
******************************************************************************/
{

    SysCtlPWMClockSet(SYSCTL_PWMDIV_32);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM0);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    GPIOPinConfigure(GPIO_PB7_M0PWM1);
    GPIOPinTypePWM(GPIO_PORTB_BASE, GPIO_PIN_7);
    PWMGenConfigure(PWM0_BASE, PWM_GEN_0, PWM_GEN_MODE_UP_DOWN |
                    PWM_GEN_MODE_NO_SYNC);
    PWMGenPeriodSet(PWM0_BASE, PWM_GEN_0, 2500);
    PWMPulseWidthSet(PWM0_BASE, PWM_OUT_1, 2500);
    PWMOutputState(PWM0_BASE, PWM_OUT_1_BIT, true);
    PWMGenEnable(PWM0_BASE, PWM_GEN_0);



	return eOk;
}


void CompareSoc(uint8_t SystemSoc)
/*****************************************************************************
*   Input    : SystemSoc
*   Output   : -
*   Function : Determine if a discharge, charge or connect cantactor. 
******************************************************************************/
{
	uint8_t ActivateCellBal = 0x01;
	uint8_t Thrashcan = 0;
	xQueueReceive(ActivateCellBalEventQueue,&Thrashcan,0);


	if ( SystemSoc > StateOfCharge )
	{
		BeginCharge();
		UARTCharPut(UART3_BASE,0xE3);
		// Charging, activate Cell Balancing 
		xQueueSendToBack(ActivateCellBalEventQueue,&ActivateCellBal,portMAX_DELAY);
		xQueueSendToBack(UpsStatusEventQueue,&SystemSoc,portMAX_DELAY);
	}else if ( SystemSoc < StateOfCharge )
	{
		BeginDisCharge( StateOfCharge-SystemSoc );
		UARTCharPut(UART3_BASE,0xE4);
		xQueueSendToBack(UpsStatusEventQueue,&SystemSoc,portMAX_DELAY);
	}else
	{
		UARTCharPut(UART3_BASE,0xE4);
		RequestUpsMode();
	}

}

void DetermineContactor(uint8_t ContactorSetting)
/*****************************************************************************
*   Input    : ContactorSetting
*   Output   : -
*   Function : If the OpMode is changed to UpSMode then connect the contactor. 
******************************************************************************/
{
	uint8_t Thrashcan = 0;
	xQueueReceive(ActivateCellBalEventQueue,&Thrashcan,0);

	if ( ContactorSetting == Connect ) // Set Connect equal to OpMode.UpsMode = TRUE.
	{
		StartSystem();
	}else
		StopSystem();

}

void Control_Task(void *pvParameters)
/*****************************************************************************
*   Input    : -
*   Output   : - 
*   Function : task to handle the input from the UpsStatusEvent
******************************************************************************/
{
	uint8_t pData = 0;
	while(1)
	{
		xQueueReceive(UpsStatusEventQueue,&pData,portMAX_DELAY);

		if ( (pData & Mode) == 0x00)
		{
			CompareSoc(pData);
		}else if ( (pData & Mode) == 0x80)
		{
			DetermineContactor((uint8_t)(pData));
		}
	}
}




/****************************** End Of Module *******************************/
