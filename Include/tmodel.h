/*****************************************************************************
* University of Southern Denmark
* 4. semester project, Electronic/Computer Engineering
*
* MODULENAME.: tmodel.h
*
* PROJECT....: Digital equalizer
*
* DESCRIPTION: Defines the elemtn of the task model..
*
* Change Log:
******************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
* 101004  MoH   Module created.
*
*****************************************************************************/

#ifndef _TMODEL_H_
#define _TMODEL_H_


// Tasks.
// ------



// Interrupt Service Routines.
// ---------------------------


// Semaphores.
// -----------



// Shared State Memory.
// --------------------


// QUEUEs.
// -------
xQueueHandle SinglePushQueue,DoublePush, LongPush;

#endif /* _TMODEL_H_ */
